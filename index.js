

let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"]

fruits.push("mango")

console.log(fruits)
// adding multiple element in an array

fruits.push("Guava", "Grapes")

console.log(fruits)

//pop()
// will remove last item in an array

let removedFruit = fruits.pop()

console.log(fruits)

//unshift()
// add element in the beginning of an array


fruits.unshift("Lime", "Banana")
console.log(fruits)

//shift ()
// removes and element at  the start of an array

let anotherFruit = fruits.shift()

console.log(fruits)

// splice()
// Simultaneously removes an element from a specified index number and adds elements
// array.splice(startingIndex, deleteCount, elementsToBeAdded)
fruits.splice(1,2,"Lime", "Cherry")
console.log(fruits)

// sort()
// rearranges the array element in alphanumeric order

fruits.sort()
console.log(fruits)

let countries = ["US", "PH", "JYP", "CA", "SG"]

// Gettin the index number starting from specified index

let lastIndexStart = countries.lastIndexOf("SG", 3)

console.log(lastIndexStart)

// slice ()
// Portions/slices elements from an array and return a new array
// Syntax arrayName.slice(startingIndex, endingIndex)

let slicedArrayA =  countries.slice(2)
console.log("Result from slice method: ")
console.log(slicedArrayA)

let slicedArrayB =  countries.slice(2, 3)
console.log("Result from slice method: ")
console.log(slicedArrayB)

// slicing off elements starting from the last elements of an array

let slicedArrayC = countries.slice(-3)
console.log("Result from slice method: ")
console.log(slicedArrayC)

// toString()
// returns an array as a string separated by commas

let stringArray = countries.toString()
console.log("Result from to String Method: ")
console.log(stringArray)

// concat()
// combines two arrays and returns a combined result
// arrayA.concat(arrayB)


let taskArray1 = ["drink html", "eat js"]
let taskArray2 = ["inhale css", "breathe sass"]
let taskArray3 = ["get git", "be node"]

let tasks = taskArray1.concat(taskArray2)
console.log("Result from concat method:")
console.log(tasks)

// combining multiple arrays
console.log("Result from concat method:")
let allTasks = taskArray1.concat(taskArray2, taskArray3)
console.log(allTasks)


//combining arrays with elements
console.log("Result from concat method:")
let combinedTasks = taskArray1.concat("smell express","throw react")
console.log(combinedTasks)

// join()
// returns an array as a specified seperator string
// arrayName.join('seperator string')

let users = ["John", "Jane", "Joe", "Robert"]

console.log(users.join())
console.log(users.join(''))

// forEach()
// similar to a for loop that iterates on each array elements
//arrayName.forEach(function(individual element))

allTasks.forEach(function(task) {
	console.log(task)
})

// using forEach with conditional statement

let filteredTasks = []
allTasks.forEach(function(task) {
	if(task.length > 10) {
		filteredTasks.push(task)
	}
})


console.log("Result from filtere Tasks:")

console.log(filteredTasks)

// map ()
// This is usefull for performing tasks where mutating/changing the elements are required
// let/const resultArray = arrayName.map(funciton(element){body})

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number){
	return number * number
})
console.log("Original Array:")
console.log(numbers)
console.log("Result from map method")

console.log(numberMap) 

// map() vs forEach()
let numberForEach = numbers.forEach(function(number){
	return number * number
})

console.log(numberForEach)

// forEach(). loop overall items in the arrayt as map(). but forEach() does not return new Array


// every()
//checks if all elements in an array meet the given condition
// resultArray = arrayName.every(function)

let allValid = numbers.every(function(number) {
	return (number < 3)
})


console.log("Result from every method:")
console.log(allValid)

// some()
// checks if at least one element in array meets the condition
// resultArray = arrayName.some(function)


let someValid = numbers.some(function(number) {
	return number < 2
})



console.log("Result from some method:")
console.log(someValid)

// filter()
// returns new array that contains elements which meet the given element
// return empty array if no elements is found


let filterValid = numbers.filter(function(number) {
	return number < 3
})

console.log("Result from filter method:")
console.log(filterValid)

//includes()
//checks if the argument passed can be found in the array
// arrayName

let products = ['Mouse', 'KeyBoard', 'Laptop', 'Monitor']

let productFound1 = products.includes('Mouse')
console.log(productFound1)

let filteredProduct = products.filter(function(product) {
	return product.toLowerCase().includes("a")
})

console.log(filteredProduct)

// reduce()
// evaluate elements from left to right and reduces the array in to a single values

//let/const resultArray = arrayName.reduce(function(accumulator, currentvalue){ ...body })

let iteration = 0

let reducedArray = numbers.reduce(function(x, y){
	console.warn("Current Iteration " + ++iteration)
	console.log("The accumulator: " + x)
	console.log("The current value: " + y)

	//the operation to reduce the array to reduce a single value
	return x + y
})

console.log('Result of reduce method: ' + reducedArray)

let list = ["Hello", "Again", "World"]

let reducedJoin = list.reduce(function (x,y) {
	return x + " " + y
})

console.log("Result of reduced method: " + reducedJoin)
